import numpy

import cv2
from skimage.io import imread, imsave


def contour_bounding_box_by_hsv_thresholds(image, low_colors, high_colors, area_threshold=0.05):
    hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    color_mask = cv2.inRange(hsv_image, low_colors, high_colors)

    # Находим контуры
    _, contours, hierarchy = cv2.findContours(color_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Отобржаем bounding boxы по контурам на картинке
    rectangles = [cv2.boundingRect(contour) for contour in contours]
    for index, rect in enumerate(rectangles):
        if rect[2] * rect[3] / (image.shape[0] * image.shape[1]) > area_threshold:
            x, y, w, h = cv2.boundingRect(contours[index])
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 1)

    # отображаем контуры поверх изображения
    cv2.drawContours(image, contours, -1, (255, 0, 0), 1, cv2.LINE_AA, hierarchy, 1)

    return image


if __name__ == "__main__":
    image = imread("../files/tmp.jpg")
    low = numpy.array([94, 80, 2])  # HSV нижний предел цвета
    high = numpy.array([126, 255, 255])  # HSV верхний предел цвета

    marked_image = contour_bounding_box_by_hsv_thresholds(image, low_colors=low, high_colors=high, area_threshold=0.05)

    imsave("../files/proc.png", marked_image)