import numpy
from skimage.io import imread, imsave


def salt_and_pepper(image: numpy.array, prop=0.05):
    """
    Add Salt-and-pepper noise to portion of pixels.
    Portion size is prop.

    TODO: Add parameter with salt and pepper ratio, instead of prop/2
    :param image: 3-D or 1-D image
    :param prop: portion of pixels with noise, float [0,1]
    :return: image with noise, 3-D or 1-D numpy array
    """
    noise_image = image.copy()
    noise = numpy.random.uniform(0, 1, image.shape[:2])
    noise = numpy.where(noise <= prop/2, -1, noise)  # Mark as -1 pepper pixel coord
    noise = numpy.where(noise > prop, 0, noise)  # Mark as 0 pixel coord without noise
    noise = numpy.where(noise > 0, 1, noise)  # Mark as 1 salt pixel coord

    if len(image.shape) == 3:
        noise = numpy.stack([noise]*image.shape[2],axis=2)
    # Apply noise to image
    noise_image = numpy.where(noise == -1, numpy.iinfo(image.dtype).min, noise_image)
    noise_image = numpy.where(noise == 1, numpy.iinfo(image.dtype).max, noise_image)

    return noise_image


def gaussian_noise(image: numpy.array, std=128):
    """
    Add Gaussian noise with mean=0, std=std.
    :param image: 3-D or 1-D image
    :param std: standard deviation
    :return: image with noise, 3-D or 1-D numpy array
    """
    noise_image = image.copy()
    noise = numpy.random.normal(loc=0, scale=std, size=image.shape)
    noise_image = noise_image + noise
    return noise_image


if __name__ == "__main__":
    image = imread("../files/tmp.jpg")
    noise = "s&p"
    kwargs = {
        "prop": 0.05
    }

    noises = {
        "gaussian": gaussian_noise,
        "s&p": salt_and_pepper
    }
    noise_image = noises.get(noise)(image, **kwargs)
    imsave("../files/noise_image.png", noise_image)