import numpy


def get_padding_size(kernel_shape: numpy.array, mode: str):
    """
    Calculate padding size. First number for vertical, second for horizontal.
    Possible variants: full, same and valid

    :param kernel_shape: size of kernel
    :param mode: one of 3 variants
    :return: padding size, numpy.array of 2 elements
    """
    kernel_shape_array = numpy.array(kernel_shape)

    if mode == 'full':
        return kernel_shape_array - 1
    if mode == 'same':
        return kernel_shape_array // 2
    if mode == 'valid':
        return numpy.array([0, 0])


def clip_padding(matrix, padding_size):
    padded_matrix = matrix.copy()

    if padding_size[0] > 0:
        v_padding_shape = list(padded_matrix.shape)
        v_padding_shape[0] = padding_size[0]
        v_padding = numpy.zeros(v_padding_shape)

        padded_matrix = numpy.vstack((v_padding, padded_matrix, v_padding))
    if padding_size[1] > 0:
        h_padding_shape = list(padded_matrix.shape)
        h_padding_shape[1] = padding_size[1]
        h_padding = numpy.zeros(h_padding_shape)

        padded_matrix = numpy.hstack((h_padding, padded_matrix, h_padding))

    return padded_matrix


def padding(image, padding_mode, padding_size):
    if padding_mode == 'clip':
        padded_image = clip_padding(image, padding_size)

    return padded_image


def filter2d(matrix, kernel, mode='valid', padding_mode='clip', stride=1):
    v_half = kernel.shape[0] // 2
    h_half = kernel.shape[1] // 2

    padding_size = get_padding_size(kernel.shape, mode)
    padded_matrix = padding(matrix, padding_mode, padding_size)

    result_matrix_shape = list(padded_matrix.shape)
    result_matrix_shape[0] = (result_matrix_shape[0] - v_half*2) // stride
    result_matrix_shape[1] = (result_matrix_shape[1] - h_half*2) // stride

    # Добавление канала для обработки grayscale изображений
    if len(matrix.shape) == 2:
        padded_matrix = padded_matrix.reshape((padded_matrix.shape[0], padded_matrix.shape[1], 1))
        result_matrix_shape.append(1)
    # Итоговая матрица
    result_matrix = numpy.zeros(result_matrix_shape)

    for row in range(v_half, padded_matrix.shape[0] - v_half, stride):
        for col in range(h_half, padded_matrix.shape[1] - h_half, stride):
            for channel in range(padded_matrix.shape[2]):
                # Перемножаем ядро свертки с частью матрицы
                conv_result = padded_matrix[row - v_half: row + v_half + 1, col - h_half: col + h_half + 1, channel] * kernel
                # Находим сумму перемноженных элементов
                conv_result = numpy.sum(conv_result)
                result_matrix[row-v_half, col-h_half, channel] = conv_result

    # Удаление канала grayscale изображения
    if len(matrix.shape) == 2:
        result_matrix = result_matrix.reshape(result_matrix.shape[0:2])

    return result_matrix
