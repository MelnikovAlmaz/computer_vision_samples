import numpy

from transform.interpolation import nn_interpolation, bilinear_interpolation


def resize(image, size, method='bilinear'):
    """
    Создает увеличенную в размерах копию исходного изображения.
    :param image: Картинка в формате 3d тензора
        Пример: [
                    [[11,11,11], [22,22,22]],
                    [[33,33,33], [100,105,150]]
                ]
    :param size: Список из 2-х элементов. Новая ширина и длинна
    :param method: Название метода интерполяции, строковое значение
    :return: point_color список из 3-ч значений RGB цвета искомой точки
    """
    interpolation = {
        'nn': nn_interpolation,
        'bilinear': bilinear_interpolation
    }

    new_image = numpy.zeros([size[0], size[1], 3])

    # Расчитываем коэфиценты уравнения соотношения координат пикселей
    def dimension_relation(old_dim, new_dim):
        a = [[-0.5, 1], [new_dim, 1]]
        b = [-0.5, old_dim + 0.5]
        cord_relation = numpy.linalg.solve(a, b)
        return cord_relation

    x_cord_coef = dimension_relation(image.shape[1], size[1])
    y_cord_coef = dimension_relation(image.shape[0], size[0])

    for row in range(size[0]):
        for column in range(size[1]):
            # Расчитываем координаты текущего пикселя
            point_column = numpy.dot(x_cord_coef, numpy.array([column, 1]))
            point_row = numpy.dot(y_cord_coef, numpy.array([row, 1]))

            # Приводим координаты пикселя к границам картинки
            point_column = numpy.clip(point_column, 0, image.shape[1] - 1)
            point_row = numpy.clip(point_row, 0, image.shape[0] - 1)

            # Находим координаты пикселей окружающих новый пиксель
            top = numpy.floor(point_row).astype(numpy.uint)
            right = numpy.ceil(point_column).astype(numpy.uint)
            bottom = numpy.ceil(point_row).astype(numpy.uint)
            left = numpy.floor(point_column).astype(numpy.uint)

            # Берем цвета ближайших точек
            nearest_colors = [
                [image[top][left], image[top][right]],
                [image[bottom][left], image[bottom][right]]
            ]

            point_cord = [numpy.modf(point_row)[0], numpy.modf(point_column)[0]]

            # Применяем интерполяцию
            new_color = interpolation.get(method)(nearest_colors, point_cord)

            new_image[row][column][:] = new_color

    return new_image.astype(numpy.uint8)