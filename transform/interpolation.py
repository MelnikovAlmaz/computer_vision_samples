def nn_interpolation(nearest_colors, point_cord):
    """
    Находит цвет точки в RGB методом ближайшего соседа
    :param nearest_colors: Матрица, где 2 строки и 2 столбаца. В ячейке (0, 0) верхняя левая точка, в (1, 1) нижняя правая.
        Пример: [
                    [[11,11,11], [22,22,22]],
                    [[33,33,33], [100,105,150]]
                ]
    :param point_cord: Кордината искомой точки внутри квадрата. Значения координат от 0 до 1
    :return: point_color список из 3-ч значений RGB цвета искомой точки
    """
    point_color = [0, 0, 0]

    row = round(point_cord[0])
    column = round(point_cord[1])

    point_color = nearest_colors[row][column].copy()
    return point_color


def bilinear_interpolation(nearest_colors, point_cord):
    """
    Находит цвет точки в RGB методом билинейной интерполяции
    :param nearest_colors: Матрица, где 2 строки и 2 столбаца. В ячейке (0, 0) верхняя левая точка, в (1, 1) нижняя правая.
        Пример: [
                    [[11,11,11], [22,22,22]],
                    [[33,33,33], [100,105,150]]
                ]
    :param point_cord: Кордината искомой точки внутри квадрата. Значения координат от 0 до 1
    :return: point_color список из 3-ч значений RGB цвета искомой точки
    """
    point_color = [0, 0, 0]

    right_color = nearest_colors[0][0] * (1 - point_cord[0]) + nearest_colors[1][0] * point_cord[0]
    left_color = nearest_colors[0][1] * (1 - point_cord[0]) + nearest_colors[1][1] * point_cord[0]

    point_color = right_color * (1 - point_cord[1]) + left_color * point_cord[1]
    point_color = numpy.round(point_color).astype(numpy.uint8)

    return point_color