import numpy
from skimage.io import imread, imsave


def cross_correlation2d(matrix, kernel, boundary='none'):
    """
    Apply 2D cross-correlation of kernel to matrix.
    :param matrix: 2D image, numpy array
    :param kernel: 2D matrix, numpy array
    :param boundary: type of boundary resolving
    :return: 2D matrix, numpy array
    """
    kernel_size = len(kernel)
    boundaries = {
        'none': lambda x, y: x.copy(),

    }
    matrix = boundaries.get(boundary)(matrix, kernel)

    result_matrix = [[0] * (matrix.shape[0] - kernel_size + 1) for _ in range((matrix.shape[0] - kernel_size + 1))]
    for i in range(matrix.shape[0] - kernel_size):
        for j in range(matrix.shape[1] - kernel_size):
            matrix_slice = matrix[i:i+kernel_size, j:j+kernel_size]
            correlation = numpy.sum(numpy.multiply(matrix_slice, kernel))
            result_matrix[i][j] = correlation

    result_matrix = numpy.array(result_matrix)
    return result_matrix


if __name__ == "__main__":
    """
    Apply sobel operator to gray-scale image as an example
    of method implementation
    """
    image = imread("../files/lenna.png")
    image = image / 255
    kernel_v = numpy.array(
        [
            [-1,-2,-1],
            [0,0,0],
            [1,2,1]
        ]
    )
    kernel_h = numpy.array(
        [
            [-1, 0, 1],
            [-2, 0, 2],
            [-1, 0, 1]
        ]
    )

    sobel_h = cross_correlation2d(image, kernel_h)
    sobel_v = cross_correlation2d(image, kernel_v)

    gradient = numpy.add(numpy.multiply(sobel_h, sobel_h), numpy.multiply(sobel_v, sobel_v))
    gradient = numpy.sqrt(gradient)
    imsave("../files/sobel_gradient.png", gradient)
