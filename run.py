import cv2
import numpy

from transform import filter2d

if __name__ == "__main__":
    image = cv2.imread("files/tmp.jpg")

    kernel = numpy.ones((5, 5)) / 25
    mode = 'same'
    padding_mode = 'clip'

    padded_image = filter2d(image, kernel, mode, padding_mode, 1)

    cv2.imwrite("files/clip.png", padded_image)